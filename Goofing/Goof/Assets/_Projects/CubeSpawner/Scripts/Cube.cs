﻿using UnityEngine;
using Archon.SwissArmyLib.Coroutines;
using System.Collections;
using DG.Tweening;
using System;

public class Cube : MonoBehaviour
{
    [SerializeField]
    private float globalCutoffTime = 10.0f;

    [Header("Settings"), SerializeField]
    private float minMoveForwardDuration = 0.5f;

    [SerializeField]
    private float maxMoveForwardDuration = 1.0f;

    [SerializeField]
    private float minMoveForwardSpeed = 5.0f;

    [SerializeField]
    private float maxMoveForwardSpeed = 10.0f;

    [SerializeField]
    private float minLeftAngle = -90.0f;

    [SerializeField]
    private float maxLeftAngle = -30.0f;

    [SerializeField]
    private float minRightAngle = 30.0f;

    [SerializeField]
    private float maxRightAngle = 90.0f;

    [Header("Components"), SerializeField]
    private GameObject cubePrefab;

    [SerializeField]
    private LayerMask cubeLayer;

    private Vector3 centerPos;

    private float internalTimer = 0.0f;
    private float moveSpeed;
    private float moveDuration;
    private bool stop = false;
    private bool hitSomething = false;

    private void Awake()
    {
        transform.SetParent(GameObject.Find("Spawner").transform);

        moveSpeed = UnityEngine.Random.Range(minMoveForwardSpeed, maxMoveForwardSpeed);
        moveDuration = UnityEngine.Random.Range(minMoveForwardDuration, maxMoveForwardDuration);
        internalTimer = Time.time + moveDuration;
        centerPos = transform.position + transform.forward * (transform.localScale.x / 2);

        TweenForward();
        //BetterCoroutines.Start(MoveForward());
    }

    private void Update()
    {
        if (hitSomething || stop)
        {
            return;
        }

        if (Time.timeSinceLevelLoad >= globalCutoffTime)
        {
            transform.DOKill();
            stop = true;
            return;
        }

        RaycastHit hit;

        if (Physics.Raycast(centerPos, transform.forward, out hit, transform.localScale.z, cubeLayer))
        {
            if (hit.transform.gameObject != gameObject)
            {
                transform.DOKill();
                hitSomething = true;
                return;
            }
        }
    }

    private void TweenForward()
    {
        float targetLength = moveSpeed * moveDuration;
        transform.DOScaleZ(targetLength, moveDuration).SetEase(Ease.OutBack).OnComplete(OnTweenForwardComplete);
    }

    private void OnTweenForwardComplete()
    {
        SpawnNext(left: true);
        SpawnNext(left: false);
    }

    private IEnumerator MoveForward()
    {
        bool hitSomething = false;
        RaycastHit hit;

        while (Time.time <= internalTimer)
        {
            if (Time.timeSinceLevelLoad >= globalCutoffTime)
            {
                stop = true;
                yield break;
            }

            Vector3 tmpScale = transform.localScale;
            tmpScale.z += moveSpeed * Time.deltaTime;
            transform.localScale = tmpScale;

            if (Physics.Raycast(centerPos, transform.forward, out hit, transform.localScale.z, cubeLayer))
            {
                if (hit.transform.gameObject != gameObject)
                {
                    hitSomething = true;
                    yield break;
                }
            }

            yield return null;
        }

        if (hitSomething || stop)
        {
            yield break;
        }

        SpawnNext(left: true);
        SpawnNext(left: false);
    }

    private void SpawnNext(bool left)
    {
        GameObject nextCube = Instantiate(cubePrefab, transform.position + (transform.forward * transform.localScale.z), transform.rotation);
        Vector3 tmpScaleLeft = nextCube.transform.localScale;
        tmpScaleLeft.z = 1.0f;
        nextCube.transform.localScale = tmpScaleLeft;
        nextCube.transform.Rotate(Vector3.up, UnityEngine.Random.Range(left ? minLeftAngle : minRightAngle, left ? maxLeftAngle : maxRightAngle));
    }
}
