﻿using UnityEngine;
using Archon.SwissArmyLib.Pooling;
using DG.Tweening;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private Cube cubePrefab;

    private void Awake()
    {
        DOTween.SetTweensCapacity(500, 50);
        Instantiate(cubePrefab);
    }
}
