﻿using UnityEngine;

public class FlowVisualizerParticles : MonoBehaviour
{
    [SerializeField]
    private float distanceThreshold = 1.0f;

    [SerializeField]
    private ParticleSystem particleSystem;

    private ParticleSystem.Particle[] particles;

    private void Awake()
    {
        particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];
    }

    private void Update()
    {
        int numOfParticlesAlive = particleSystem.GetParticles(particles);

        for (int i = 0; i < numOfParticlesAlive; i++)
        {
            ParticleSystem.Particle particle1 = particles[i];

            for (int j = i + 1; j < numOfParticlesAlive; j++)
            {
                ParticleSystem.Particle particle2 = particles[j];

                if(Vector2.Distance(transform.TransformPoint(particle1.position), transform.TransformPoint(particle2.position)) < distanceThreshold)
                {
                    Debug.DrawLine(transform.TransformPoint(particle1.position), transform.TransformPoint(particle2.position), Color.blue);
                }
            }
        }

        //particleSystem.SetParticles(particles, numOfParticlesAlive);
    }
}
