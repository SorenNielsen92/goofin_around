﻿using UnityEngine;

public class FlowVisualizer : MonoBehaviour
{
    [SerializeField]
    private int pointCount = 100;

    [SerializeField]
    private float maxSpread = 5.0f;

    [SerializeField]
    private float pointSize = 0.1f;

    [SerializeField]
    private float distanceThreshold = 1.0f;

    private Vector2[] points = new Vector2[0];

    private void Awake()
    {
        InitializePoints();
    }

    private void InitializePoints()
    {
        points = new Vector2[pointCount];

        for (int i = 0; i < points.Length; i++)
        {
            points[i] = Random.insideUnitCircle * maxSpread;
        }
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < points.Length; i++)
        {
            Vector2 firstPoint = points[i];

            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(firstPoint, pointSize);

            for (int j = i + 1; j < points.Length; j++)
            {
                Vector2 secondPoint = points[j];

                if(Vector2.Distance(firstPoint, secondPoint) < distanceThreshold)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawLine(firstPoint, secondPoint);
                }
            }
        }
    }
}
