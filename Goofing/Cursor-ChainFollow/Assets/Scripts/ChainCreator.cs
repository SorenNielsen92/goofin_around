﻿using UnityEngine;

public class ChainCreator : MonoBehaviour
{
    [SerializeField]
    private ChainJoint jointPrefab;

    [SerializeField]
    private int jointsAmount = 6;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;

        ChainJoint joint = Instantiate(jointPrefab);
        joint.Setup(jointsAmount, null);
    }
}