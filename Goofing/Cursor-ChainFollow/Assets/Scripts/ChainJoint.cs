﻿using UnityEngine;

public class ChainJoint : MonoBehaviour
{
    [SerializeField]
    private float followSmoothing = 6.0f;

    [SerializeField]
    private bool incrementalSmoothing = false;

    [SerializeField]
    private float distanceBetweenJoints = 2.0f;

    [SerializeField]
    private MeshRenderer render;

    private Transform parent;

    private Vector3 targetPos;
    private Vector3 initScale;

    private Quaternion targetRot;

    public void Setup(int chainsLeft, Transform parent)
    {
        //Do initial setup
        if (parent != null)
        {
            this.parent = parent;
            transform.position = parent.position + Vector3.forward * distanceBetweenJoints;
        }

        initScale = transform.localScale;

        if (incrementalSmoothing)
        {
            //followSmoothing += Mathf.Pow(2, 2) * 0.5f;
            followSmoothing += 1;
        }

        //Break recursive
        if (chainsLeft <= 0)
        {
            return;
        }

        //Spawn next chain
        ChainJoint nextJoint = Instantiate(this);
        nextJoint.Setup(--chainsLeft, transform);
    }

    private void Update()
    {
        if (parent == null)
        {
            targetPos = Input.mousePosition;
            targetPos.z = 10.0f;
            targetPos = Camera.main.ScreenToWorldPoint(targetPos);

            //targetPos.x = 4.0f;
            //targetPos.y = 4.0f * Mathf.Sin(Time.time * 3.0f);
            //targetRot = Quaternion.Euler(transform.position.y > 0 ? -30.0f : 30.0f, transform.eulerAngles.y, transform.eulerAngles.z);
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 1.5f);
        }
        else
        {
            targetPos = parent.position;
            //targetRot = parent.rotation;
        }

        transform.position = Vector3.Lerp(transform.position, new Vector3(targetPos.x, targetPos.y, transform.position.z), Time.deltaTime * followSmoothing);

        //if (parent != null)
        //{
        //    transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * followSmoothing * 0.5f);
        //}
    }
}
