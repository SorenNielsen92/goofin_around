﻿using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private CharacterController2D controller;

    [SerializeField]
    private UIInfo bottomInfo;

    [SerializeField]
    private UIInfo topInfo;

    [SerializeField]
    private UIInfo horizontalInfo;

    [SerializeField]
    private UIInfo slopeInfo;

    [SerializeField]
    private UIInfo ascendSlopeInfo;

    [SerializeField]
    private UIInfo descendSlopeInfo;

    [SerializeField]
    private UIInfo maxSlopeInfo;

    [SerializeField]
    private UIInfo slopeAngleInfo;

    private void Start()
    {
        bottomInfo.SetInfoField("Collision Bottom:");
        topInfo.SetInfoField("Collision Top:");
        horizontalInfo.SetInfoField("Collision Horizontal:");
        slopeInfo.SetInfoField("Slope:");
        ascendSlopeInfo.SetInfoField("Ascending Slope:");
        descendSlopeInfo.SetInfoField("Descending Slope:");
        maxSlopeInfo.SetInfoField("Collision Slope Max Angle:");
        slopeAngleInfo.SetInfoField("Slope Angle:");
    }

    private void Update()
    {
        bottomInfo.SetDataField(controller.CollisionInfoData.Bottom);
        topInfo.SetDataField(controller.CollisionInfoData.Top);
        horizontalInfo.SetDataField(controller.CollisionInfoData.Horizontal);
        slopeInfo.SetDataField(controller.CollisionInfoData.Slope);
        ascendSlopeInfo.SetDataField(controller.CollisionInfoData.AscendingSlope);
        descendSlopeInfo.SetDataField(controller.CollisionInfoData.DescendingSlope);
        maxSlopeInfo.SetDataField(controller.CollisionInfoData.ExceedingMaxSlope);
        slopeAngleInfo.SetDataField(controller.CollisionInfoData.SlopeAngle);
    }
}
