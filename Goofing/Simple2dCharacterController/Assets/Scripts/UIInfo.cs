﻿using UnityEngine;
using UnityEngine.UI;

public class UIInfo : MonoBehaviour
{
    [SerializeField]
    private Text infoTextField;

    [SerializeField]
    private Text dataTextField;

    public void SetInfoField(string name)
    {
        infoTextField.text = name;
    }

    public void SetDataField(bool value)
    {
        dataTextField.text = value ? "True" : "False";
        dataTextField.color = value ? Color.green : Color.red;
    }

    public void SetDataField(float value)
    {
        dataTextField.text = value.ToString();
        dataTextField.color = Color.yellow;
    }
}
