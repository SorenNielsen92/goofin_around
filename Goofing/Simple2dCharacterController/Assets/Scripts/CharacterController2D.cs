﻿using System.Collections.Generic;
using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
    [Header("Movement"), SerializeField]
    private float moveSpeed = 10.0f;

    [SerializeField]
    private float moveAcceleration = 40.0f;

    [SerializeField]
    private float deceleration = 100.0f;

    [Header("Jumping"), SerializeField]
    private float jumpHeight = 3.5f;

    [SerializeField]
    private float airAcceleration = 40.0f;

    [SerializeField]
    private float airDeceleration = 25.0f;

    [Header("Slopes"), SerializeField]
    private float maxSlopeAngle = 45.0f;

    [Header("Settings"), SerializeField]
    private float horizontalCollisionPadding = -0.01f;

    [SerializeField]
    private BoxCollider2D boxCollider;

    [SerializeField]
    private LayerMask groundMask;

    [Header("Animation"), SerializeField]
    private Animator anim;

    [SerializeField]
    private Vector3 velocity;

    private float horizontal;
    private float signInput;
    private float signVelocity;

    private bool jumping;

    private Collider2D[] hits;
    private List<ColliderDistance2D> distanceInfos = new List<ColliderDistance2D>();

    public CollisionInfo CollisionInfoData;

    private void Update()
    {
        HandleJumping();
        ApplyGravity();
        HandleMovement();
        HandleCollision();
        UpdateAnimations();
    }

    private void HandleJumping()
    {
        if (CollisionInfoData.Bottom)
        {
            jumping = false;

            velocity.y = 0;

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));
                jumping = true;

                anim.SetTrigger("Jump");
            }
        }
    }

    private void ApplyGravity()
    {
        if (!CollisionInfoData.AscendingSlope)
        {
            velocity.y += Physics2D.gravity.y * Time.deltaTime;
        }
    }

    private void HandleMovement()
    {
        horizontal = Input.GetAxisRaw("Horizontal");

        signInput = Mathf.Sign(horizontal);
        signVelocity = Mathf.Sign(velocity.x);

        float actualAcceleration = CollisionInfoData.Bottom ? moveAcceleration : airAcceleration;
        float actualDeceleration = CollisionInfoData.Bottom ? deceleration : airDeceleration;

        //Decelerate properly, when player is abruptly changing direction
        if (signInput != signVelocity && Mathf.Abs(velocity.x) > 0.0f && CollisionInfoData.Bottom)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0.0f, actualDeceleration * Time.deltaTime);
        }
        else
        {
            //Accelerate movement
            if (horizontal != 0.0f)
            {
                velocity.x = Mathf.MoveTowards(velocity.x, moveSpeed * horizontal, actualAcceleration * Time.deltaTime);
            }
            //Decelerate movement when letting go of input
            else
            {
                velocity.x = Mathf.MoveTowards(velocity.x, 0.0f, actualDeceleration * Time.deltaTime);
            }
        }

        transform.Translate(velocity * Time.deltaTime);
    }

    private void HandleCollision()
    {
        CollisionInfoData.Reset();

        float absVelX = Mathf.Abs(velocity.x);

        hits = Physics2D.OverlapBoxAll(transform.position, new Vector2(boxCollider.size.x + horizontalCollisionPadding, boxCollider.size.y), 0, groundMask);
        distanceInfos.Clear();

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == boxCollider)
            {
                continue;
            }

            ColliderDistance2D distance = hits[i].Distance(boxCollider);
            distanceInfos.Add(distance);

            distanceInfos.Add(distance);

            //is a collision happening
            if (distance.isOverlapped)
            {
                CollisionInfoData.SlopeAngle = Vector2.Angle(distance.normal, Vector2.up);

                if (CollisionInfoData.SlopeAngle == 0.0f)
                {
                    CollisionInfoData.Bottom = true;
                    velocity.y = 0.0f;

                    transform.Translate(distance.pointA - distance.pointB);
                }
                else if (CollisionInfoData.SlopeAngle > 90.0f && Mathf.Sign(velocity.y) > 0.0f)
                {
                    CollisionInfoData.Top = true;
                    velocity.y = 0.0f;

                    transform.Translate(distance.pointA - distance.pointB);
                }
                else if (CollisionInfoData.SlopeAngle >= maxSlopeAngle)
                {
                    CollisionInfoData.Horizontal = true;
                    velocity.x = 0.0f;

                    transform.Translate(distance.pointA - distance.pointB);
                }
                else if (CollisionInfoData.SlopeAngle <= maxSlopeAngle)
                {
                    CollisionInfoData.Slope = true;
                    CollisionInfoData.Bottom = true;
                    velocity.y = 0.0f;

                    if (horizontal != 0.0f)
                    {
                        if (Mathf.Sign(distance.normal.x) != Mathf.Sign(velocity.x))
                        {
                            CollisionInfoData.AscendingSlope = true;
                            transform.Translate(Vector3.up * (distance.pointA - distance.pointB));
                        }
                        else
                        {
                            CollisionInfoData.DescendingSlope = true;
                        }
                    }
                    else
                    {
                        transform.Translate(Vector3.up * (distance.pointA - distance.pointB));
                    }
                }

                #region OLD_COLLISION_CODE
                ////Check for collision with slope
                //if (CollisionInfoData.SlopeAngle > 0.0f && CollisionInfoData.SlopeAngle <= maxSlopeAngle)
                //{
                //    CollisionInfoData.AscendingSlope = true;
                //    CollisionInfoData.Bottom = true;

                //    //If the player is moving horizontally OR the distance between the slope object and player object is greater than 0.1f
                //    if (absVelX > 0.0f || Vector3.Distance(distance.pointA, distance.pointB) > 0.1f)
                //    {
                //        //If we are moving, shift the player out of the overlapping colliders
                //        if (absVelX > 0.0f)
                //        {
                //            transform.Translate(distance.pointA - distance.pointB);
                //        }
                //        else
                //        {
                //            //If we are NOT moving, so landing after a jump, only move the player upwards from the slope object to prevent jittering when landing on slopes.
                //            transform.Translate(Vector3.up * (distance.pointA - distance.pointB));
                //        }
                //    }
                //}
                ////Are we actually getting an angle that goes above the accepted slope angle ?
                //else if (CollisionInfoData.SlopeAngle > maxSlopeAngle && CollisionInfoData.SlopeAngle < 90.0f && absVelX > 0.0f && CollisionInfoData.SlopeAngle > 0.0f)
                //{
                //    CollisionInfoData.Bottom = false;
                //    CollisionInfoData.ExceedingMaxSlope = true;

                //    transform.Translate(distance.pointA - distance.pointB);

                //    velocity.x = 0.0f;
                //    //horizontal = 0.0f;
                //}
                //else
                //{
                //    //Collide normally
                //    transform.Translate(distance.pointA - distance.pointB);
                //}

                ////Check if we collide with ground
                //if (CollisionInfoData.SlopeAngle == 0.0f && velocity.y < 0.0f)
                //{
                //    CollisionInfoData.Bottom = true;
                //}
                ////Check if we have bumped into ceiling
                //else if (CollisionInfoData.SlopeAngle > 90.0f && Mathf.Sign(velocity.y) > 0.0f)
                //{
                //    CollisionInfoData.Top = true;
                //    velocity.y = 0.0f;
                //}
                ////Check if we have hit a wall on either side
                //else if (CollisionInfoData.SlopeAngle == 90.0f && Mathf.Abs(horizontal) > 0.0f)
                //{
                //    CollisionInfoData.Horizontal = true;
                //    velocity.x = 0.0f;
                //} 
                #endregion
            }
        }

        //Always check for descend slope.
        //Pretty ineffecient, but needed, otherwise player will jitter down slopes and loose contact with collider (ground/slope)
        DescendSlope();
    }

    private void DescendSlope()
    {
        //Descending slopes
        float signVelX = Mathf.Sign(velocity.x);
        Vector2 collidingCorner = transform.position + new Vector3(signVelX * -boxCollider.bounds.extents.x, -boxCollider.bounds.extents.y, 0.0f);
        RaycastHit2D slopeHit = Physics2D.Raycast(collidingCorner, Vector2.down, Mathf.Infinity, groundMask);

        CollisionInfoData.SlopeAngle = Vector2.Angle(slopeHit.normal, Vector2.up);

        //Check if slope angle is viable
        if (CollisionInfoData.SlopeAngle > 0.0f && CollisionInfoData.SlopeAngle <= maxSlopeAngle && !jumping && !CollisionInfoData.Bottom && slopeHit.distance <= 0.1f)
        {
            //Check if we are going opposite direction of slope direction, aka descending
            if (Mathf.Sign(slopeHit.normal.x) == signVelX)
            {
                transform.Translate(slopeHit.point - collidingCorner);

                CollisionInfoData.AscendingSlope = false;
                CollisionInfoData.Bottom = true;
                CollisionInfoData.DescendingSlope = true;
            }
        }
    }

    private void UpdateAnimations()
    {
        anim.SetBool("Grounded", CollisionInfoData.Bottom);
        anim.SetFloat("Speed", Mathf.Abs(horizontal));
    }

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < distanceInfos.Count; i++)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(distanceInfos[i].pointA, distanceInfos[i].normal * 0.5f);

            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(distanceInfos[i].pointA, (distanceInfos[i].pointA - distanceInfos[i].pointB));
        }

        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, velocity);

        Gizmos.DrawSphere(transform.position + new Vector3(0.5f, -1.0f, 0.0f), 0.1f);
    }
}

[System.Serializable]
public struct CollisionInfo
{
    public bool Bottom;
    public bool Top;
    public bool Horizontal;
    public bool Slope;
    public bool AscendingSlope;
    public bool DescendingSlope;
    public bool ExceedingMaxSlope;

    public float SlopeAngle;

    public void Reset()
    {
        Bottom = false;
        Top = false;
        Horizontal = false;
        Slope = false;
        AscendingSlope = false;
        DescendingSlope = false;
        ExceedingMaxSlope = false;

        SlopeAngle = 0.0f;
    }
}