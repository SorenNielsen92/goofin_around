// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1719,x:32844,y:32925,varname:node_1719,prsc:2|diff-8011-OUT,clip-2856-OUT;n:type:ShaderForge.SFN_Panner,id:5610,x:31120,y:32860,varname:node_5610,prsc:2,spu:0,spv:0.1|UVIN-3560-UVOUT,DIST-6956-OUT;n:type:ShaderForge.SFN_TexCoord,id:3560,x:30926,y:32846,varname:node_3560,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:7395,x:30734,y:33035,varname:node_7395,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:7778,x:30751,y:32922,ptovrint:False,ptlb:ScrollSpeed,ptin:_ScrollSpeed,varname:node_7778,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:6956,x:30926,y:33015,varname:node_6956,prsc:2|A-7778-OUT,B-7395-T;n:type:ShaderForge.SFN_Tex2d,id:3911,x:31307,y:32886,ptovrint:False,ptlb:noise,ptin:_noise,varname:node_3911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-5610-UVOUT;n:type:ShaderForge.SFN_Add,id:8025,x:31541,y:33059,varname:node_8025,prsc:2|A-3911-R,B-6293-G;n:type:ShaderForge.SFN_Panner,id:6100,x:31129,y:33313,varname:node_6100,prsc:2,spu:0,spv:0.1|UVIN-6016-UVOUT,DIST-7737-OUT;n:type:ShaderForge.SFN_TexCoord,id:6016,x:30926,y:33414,varname:node_6016,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:4145,x:30734,y:33219,varname:node_4145,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:2536,x:30746,y:33414,ptovrint:False,ptlb:ScrollSpeed2,ptin:_ScrollSpeed2,varname:_ScrollSpeed_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:7737,x:30926,y:33249,varname:node_7737,prsc:2|A-2536-OUT,B-4145-T;n:type:ShaderForge.SFN_Tex2d,id:6293,x:31302,y:33255,ptovrint:False,ptlb:noise2,ptin:_noise2,varname:_node_3911_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-6100-UVOUT;n:type:ShaderForge.SFN_Color,id:1047,x:32051,y:32381,ptovrint:False,ptlb:Color_Foam,ptin:_Color_Foam,varname:node_7575,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:7216,x:32051,y:32578,ptovrint:False,ptlb:Color_Water,ptin:_Color_Water,varname:node_6258,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4862745,c2:0.7372549,c3:0.9960785,c4:1;n:type:ShaderForge.SFN_Lerp,id:5162,x:32561,y:32585,varname:node_5162,prsc:2|A-1047-RGB,B-7216-RGB,T-8857-OUT;n:type:ShaderForge.SFN_Subtract,id:8890,x:31890,y:32911,varname:node_8890,prsc:2|A-8025-OUT,B-3655-OUT;n:type:ShaderForge.SFN_Divide,id:1328,x:32111,y:32926,varname:node_1328,prsc:2|A-8890-OUT,B-4364-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3655,x:31661,y:33230,ptovrint:False,ptlb:Lower_End,ptin:_Lower_End,varname:node_3655,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.7;n:type:ShaderForge.SFN_ValueProperty,id:1398,x:31717,y:33356,ptovrint:False,ptlb:Upper_End,ptin:_Upper_End,varname:node_1398,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Subtract,id:4364,x:31898,y:33174,varname:node_4364,prsc:2|A-1398-OUT,B-3655-OUT;n:type:ShaderForge.SFN_Lerp,id:4286,x:32310,y:33050,varname:node_4286,prsc:2|A-1328-OUT,B-1328-OUT,T-9132-RGB;n:type:ShaderForge.SFN_VertexColor,id:9132,x:32156,y:33297,varname:node_9132,prsc:2;n:type:ShaderForge.SFN_Add,id:5079,x:32699,y:32717,varname:node_5079,prsc:2|A-5162-OUT,B-9132-RGB;n:type:ShaderForge.SFN_Clamp01,id:8011,x:32660,y:32883,varname:node_8011,prsc:2|IN-5079-OUT;n:type:ShaderForge.SFN_Clamp01,id:8857,x:32371,y:32739,varname:node_8857,prsc:2|IN-4286-OUT;n:type:ShaderForge.SFN_OneMinus,id:2856,x:32490,y:33272,varname:node_2856,prsc:2|IN-9132-B;proporder:7778-3911-2536-6293-1047-7216-3655-1398;pass:END;sub:END;*/

Shader "Unlit/Waterfall" {
    Properties {
        _ScrollSpeed ("ScrollSpeed", Float ) = 0.5
        _noise ("noise", 2D) = "white" {}
        _ScrollSpeed2 ("ScrollSpeed2", Float ) = 0.5
        _noise2 ("noise2", 2D) = "white" {}
        _Color_Foam ("Color_Foam", Color) = (1,1,1,1)
        _Color_Water ("Color_Water", Color) = (0.4862745,0.7372549,0.9960785,1)
        _Lower_End ("Lower_End", Float ) = 0.7
        _Upper_End ("Upper_End", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 100
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _ScrollSpeed;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float _ScrollSpeed2;
            uniform sampler2D _noise2; uniform float4 _noise2_ST;
            uniform float4 _Color_Foam;
            uniform float4 _Color_Water;
            uniform float _Lower_End;
            uniform float _Upper_End;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                clip((1.0 - i.vertexColor.b) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 node_7395 = _Time;
                float2 node_5610 = (i.uv0+(_ScrollSpeed*node_7395.g)*float2(0,0.1));
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(node_5610, _noise));
                float4 node_4145 = _Time;
                float2 node_6100 = (i.uv0+(_ScrollSpeed2*node_4145.g)*float2(0,0.1));
                float4 _noise2_var = tex2D(_noise2,TRANSFORM_TEX(node_6100, _noise2));
                float node_1328 = (((_noise_var.r+_noise2_var.g)-_Lower_End)/(_Upper_End-_Lower_End));
                float3 node_4286 = lerp(float3(node_1328,node_1328,node_1328),float3(node_1328,node_1328,node_1328),i.vertexColor.rgb);
                float3 diffuseColor = saturate((lerp(_Color_Foam.rgb,_Color_Water.rgb,saturate(node_4286))+i.vertexColor.rgb));
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                clip((1.0 - i.vertexColor.b) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
