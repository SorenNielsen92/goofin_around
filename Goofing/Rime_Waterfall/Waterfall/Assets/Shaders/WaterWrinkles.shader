// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:0,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1719,x:32823,y:32667,varname:node_1719,prsc:2|diff-4450-RGB,clip-4039-OUT;n:type:ShaderForge.SFN_Panner,id:5610,x:31822,y:32753,varname:node_5610,prsc:2,spu:0.1,spv:0|UVIN-3560-UVOUT,DIST-6956-OUT;n:type:ShaderForge.SFN_TexCoord,id:3560,x:31628,y:32739,varname:node_3560,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:7395,x:31436,y:32928,varname:node_7395,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:7778,x:31453,y:32815,ptovrint:False,ptlb:ScrollSpeed,ptin:_ScrollSpeed,varname:node_7778,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:6956,x:31628,y:32908,varname:node_6956,prsc:2|A-7778-OUT,B-7395-T;n:type:ShaderForge.SFN_Tex2d,id:3911,x:32009,y:32779,ptovrint:False,ptlb:noise,ptin:_noise,varname:node_3911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:47972103210e4e54786fc0e060eb26db,ntxv:0,isnm:False|UVIN-5610-UVOUT;n:type:ShaderForge.SFN_Color,id:4450,x:32325,y:32648,ptovrint:False,ptlb:base_color,ptin:_base_color,varname:node_4450,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Add,id:8025,x:32325,y:33021,varname:node_8025,prsc:2|A-3911-R,B-6293-G;n:type:ShaderForge.SFN_Panner,id:6100,x:31831,y:33206,varname:node_6100,prsc:2,spu:0.1,spv:0|UVIN-6016-UVOUT,DIST-7737-OUT;n:type:ShaderForge.SFN_TexCoord,id:6016,x:31628,y:33307,varname:node_6016,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:4145,x:31436,y:33112,varname:node_4145,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:2536,x:31448,y:33307,ptovrint:False,ptlb:ScrollSpeed2,ptin:_ScrollSpeed2,varname:_ScrollSpeed_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:7737,x:31628,y:33142,varname:node_7737,prsc:2|A-2536-OUT,B-4145-T;n:type:ShaderForge.SFN_Tex2d,id:6293,x:32004,y:33148,ptovrint:False,ptlb:noise2,ptin:_noise2,varname:_node_3911_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:47972103210e4e54786fc0e060eb26db,ntxv:0,isnm:False|UVIN-6100-UVOUT;n:type:ShaderForge.SFN_VertexColor,id:8586,x:32325,y:32842,varname:node_8586,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4039,x:32528,y:32876,varname:node_4039,prsc:2|A-8586-RGB,B-8025-OUT;proporder:4450-7778-3911-2536-6293;pass:END;sub:END;*/

Shader "Unlit/WaterWrinkles" {
    Properties {
        _base_color ("base_color", Color) = (1,1,1,1)
        _ScrollSpeed ("ScrollSpeed", Float ) = 0.5
        _noise ("noise", 2D) = "white" {}
        _ScrollSpeed2 ("ScrollSpeed2", Float ) = 0.5
        _noise2 ("noise2", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        LOD 100
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float _ScrollSpeed;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float4 _base_color;
            uniform float _ScrollSpeed2;
            uniform sampler2D _noise2; uniform float4 _noise2_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float4 node_7395 = _Time;
                float2 node_5610 = (i.uv0+(_ScrollSpeed*node_7395.g)*float2(0.1,0));
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(node_5610, _noise));
                float4 node_4145 = _Time;
                float2 node_6100 = (i.uv0+(_ScrollSpeed2*node_4145.g)*float2(0.1,0));
                float4 _noise2_var = tex2D(_noise2,TRANSFORM_TEX(node_6100, _noise2));
                clip((i.vertexColor.rgb*(_noise_var.r+_noise2_var.g)) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = _base_color.rgb;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _ScrollSpeed;
            uniform sampler2D _noise; uniform float4 _noise_ST;
            uniform float _ScrollSpeed2;
            uniform sampler2D _noise2; uniform float4 _noise2_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_7395 = _Time;
                float2 node_5610 = (i.uv0+(_ScrollSpeed*node_7395.g)*float2(0.1,0));
                float4 _noise_var = tex2D(_noise,TRANSFORM_TEX(node_5610, _noise));
                float4 node_4145 = _Time;
                float2 node_6100 = (i.uv0+(_ScrollSpeed2*node_4145.g)*float2(0.1,0));
                float4 _noise2_var = tex2D(_noise2,TRANSFORM_TEX(node_6100, _noise2));
                clip((i.vertexColor.rgb*(_noise_var.r+_noise2_var.g)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
