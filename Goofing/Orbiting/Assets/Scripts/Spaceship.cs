﻿using UnityEngine;

public class Spaceship : MonoBehaviour
{
    [Header("Forces"), SerializeField]
    private float springForce = 20.0f;

    [SerializeField]
    private float damping = 3.0f;

    [SerializeField]
    private float forwardThrusterForce = 1.0f;

    [SerializeField]
    private float boosterForce = 10.0f;

    [Header("Planet"), SerializeField]
    private Planet currentOrbitingPlanet;

    [Header("Graphics"), SerializeField]
    private Transform graphics;

    [Header("DEBUG"), SerializeField]
    private Vector3 currentVelocity;
    [SerializeField]
    private Vector3 planetDirection;
    [SerializeField]
    private float currentPlanetLength;
    [SerializeField]
    private float currentLengthFromOrbit;
    [SerializeField]
    private float startLengthFromOrbit;
    [SerializeField]
    private float percentageAwayFromOrbit;
    [SerializeField]
    private float variedSpringForce;

    private bool springAttached = true;

    private void Awake()
    {
        startLengthFromOrbit = (transform.position - currentOrbitingPlanet.transform.position).magnitude;
    }

    private void Update()
    {
        planetDirection = transform.position - currentOrbitingPlanet.transform.position;
        currentPlanetLength = planetDirection.magnitude;
        currentLengthFromOrbit = Mathf.Abs(currentPlanetLength - currentOrbitingPlanet.OrbitDistance);

        UpdateInput();

        if (springAttached)
        {
            currentVelocity = UpdateSpringForces();
            currentVelocity += MoveForward();
            UpdateGraphicsRotation(currentLengthFromOrbit);
        }

        transform.rotation = CalculateRotation();
        transform.position += currentVelocity * Time.deltaTime;
    }

    private Vector3 MoveForward()
    {
        return transform.up * forwardThrusterForce * Time.deltaTime ;
    }

    private Quaternion CalculateRotation()
    {
        float angle = Mathf.Atan2(-planetDirection.y, -planetDirection.x) * Mathf.Rad2Deg;
        Quaternion targetRot = Quaternion.AngleAxis(angle, Vector3.forward);
        //Quaternion targetRot = Quaternion.Euler(new Vector3(0.0f, 0.0f, angle));

        return targetRot;
    }

    //Fix direction later. Right now it's only positive because of Mathf.Abs
    private void UpdateGraphicsRotation(float direction)
    {
        Quaternion target = Quaternion.Euler(new Vector3(0.0f, 0.0f, direction * 5.0f));
        graphics.localRotation = Quaternion.Slerp(graphics.localRotation, target, Time.deltaTime * 40.0f);
    }

    private Vector3 UpdateSpringForces()
    {
        Vector3 velocity = currentVelocity;
        Vector3 displacement = transform.position - (planetDirection.normalized * currentOrbitingPlanet.OrbitDistance);

        float maxDistance = Mathf.Abs(startLengthFromOrbit - currentOrbitingPlanet.OrbitDistance);
        percentageAwayFromOrbit = currentLengthFromOrbit / maxDistance;

        //Spring
        //variedSpringForce = ((1 - percentageAwayFromOrbit) + springForce * 0.5f) * springForce;
        velocity -= displacement * springForce * Time.deltaTime;

        //Damping
        velocity = velocity * (1.0f - damping * Time.deltaTime);

        return velocity;
    }

    private void UpdateInput()
    {
        if (Input.GetKey(KeyCode.RightArrow) && springAttached)
        {
            currentVelocity += transform.up * boosterForce * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            springAttached = !springAttached;

            //if(!springAttached)
            //{
            //    currentOrbitingPlanet = GameObject.Find("Planet_02").GetComponent<Planet>();
            //}
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(currentOrbitingPlanet.transform.position, transform.position);
        Gizmos.color = Color.white;
        Gizmos.DrawRay(currentOrbitingPlanet.transform.position, planetDirection.normalized * currentOrbitingPlanet.OrbitDistance);
    }
}
