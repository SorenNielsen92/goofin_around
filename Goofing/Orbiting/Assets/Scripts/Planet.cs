﻿using UnityEngine;

public class Planet : MonoBehaviour
{
    [SerializeField]
    private float orbitDistance = 8.0f;

    public float OrbitDistance
    {
        get
        {
            return orbitDistance;
        }
    }
}
