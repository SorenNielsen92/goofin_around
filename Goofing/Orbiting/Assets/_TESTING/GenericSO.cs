﻿using UnityEngine;

public class GenericSO<T> : ScriptableObject where T : Planet
{
    public T Data;
}
