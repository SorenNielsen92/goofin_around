﻿using UnityEngine;

[ExecuteInEditMode]
public class GraphGrid : MonoBehaviour
{
    [Header("Grid Settings"), SerializeField]
    private Vector3 gridOrigin = Vector3.zero;

    [SerializeField]
    private int gridSizeX = 10;

    [SerializeField]
    private int gridSizeY = 10;

    [SerializeField]
    private int gridDimensions = 1;

    [SerializeField]
    private Color gridLineColor = Color.blue;

    [SerializeField]
    private Material gridMaterial;

    [Header("Vector Settings"), SerializeField]
    private Vector3 vectorStart;

    [SerializeField]
    private Vector3 vectorEnd;

    [SerializeField]
    private Color vectorColor = Color.red;

    [SerializeField]
    private Material vectorMaterial;

    private void OnPostRender()
    {
        RenderGLLines();
    }

    private void OnDrawGizmos()
    {
        RenderGLLines();

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(Vector3.zero, 0.3f);
    }

    private void RenderGLLines()
    {
        if (gridMaterial == null || vectorMaterial == null)
        {
            return;
        }

        Vector3 start = Vector3.zero;
        Vector3 end = Vector3.zero;

        GL.PushMatrix();

        //Grid
        GL.Begin(GL.LINES);

        gridMaterial.SetPass(0);
        gridMaterial.color = gridLineColor;

        for (int x = -gridSizeX; x <= gridSizeX; x++)
        {
            start = gridOrigin + new Vector3(gridOrigin.x - (gridDimensions * x), gridOrigin.y - (gridSizeY * gridDimensions), 0.0f);
            end = start + Vector3.up * ((gridSizeY * 2) * gridDimensions);

            GL.Vertex(start);
            GL.Vertex(end);
        }

        for (int y = -gridSizeY; y <= gridSizeY; y++)
        {
            start = gridOrigin + new Vector3(gridOrigin.x - (gridSizeX * gridDimensions), gridOrigin.y - (gridDimensions * y), 0.0f);
            end = start + Vector3.right * ((gridSizeX * 2) * gridDimensions);

            GL.Vertex(start);
            GL.Vertex(end);
        }

        GL.End();

        ////Vectors
        GL.Begin(GL.LINES);
        vectorMaterial.SetPass(0);
        vectorMaterial.color = vectorColor;

        GL.Vertex((gridOrigin + vectorStart) * gridDimensions);
        GL.Vertex((gridOrigin + vectorEnd) * gridDimensions);

        GL.End();

        GL.PopMatrix();







        //float startX = 0.0f;
        //float startY = 0.0f;
        //float startZ = 0.0f;

        //float offsetX = 0.0f;
        //float offsetY = 0.0f;

        ////Grid
        //GL.Begin(GL.LINES);

        //gridMaterial.SetPass(0);
        //gridMaterial.color = gridLineColor;

        ////Layers
        //for (float j = 0; j <= gridSizeY; j += gridDimensions)
        //{
        //    //X axis lines
        //    for (float i = 0; i <= gridSizeZ; i += gridDimensions)
        //    {
        //        GL.Vertex3(startX, j + offsetY, startZ + i);
        //        GL.Vertex3(gridSizeX, j + offsetY, startZ + i);
        //    }

        //    //Z axis lines
        //    for (float i = 0; i <= gridSizeX; i += gridDimensions)
        //    {
        //        GL.Vertex3(startX + i, j + offsetY, startZ);
        //        GL.Vertex3(startX + i, j + offsetY, gridSizeZ);
        //    }
        //}

        ////Y axis lines
        //for (float i = 0; i <= gridSizeZ; i += gridDimensions)
        //{
        //    for (float k = 0; k <= gridSizeX; k += gridDimensions)
        //    {
        //        GL.Vertex3(startX + k, startY + offsetY, startZ + i);
        //        GL.Vertex3(startX + k, gridSizeY + offsetY, startZ + i);
        //    }
        //}

        //GL.End();
    }
}
